import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { globalStyles } from '../styles/global';

export default function Organization() {
  return (
    <View style={globalStyles.container}>
      <Text style={globalStyles.titleText}>Organization Screen</Text>
    </View>
  );
}